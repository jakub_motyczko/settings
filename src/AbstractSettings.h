/*
 * Copyright (C) MOTiCO All Rights Reserved
 *
 * ** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of Digia Plc and its Subsidiary(-ies) nor the names
**     of its contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 *
 * Written by Jakub Motyczko <jakub@motyczko.pl>
 */

#ifndef ABSTRACTSETTINGS_H
#define ABSTRACTSETTINGS_H

#include <QSettings>
#include <QMutex>
#include <QMutexLocker>

#define SETTING(TYPE, NAME, DEFAULT_VALUE) \
    SETTING_PATH("", TYPE, NAME, DEFAULT_VALUE)

#define SETTING_PATH(PATH, TYPE, NAME, DEFAULT_VALUE)\
    Q_PROPERTY(TYPE NAME READ NAME WRITE set_##NAME NOTIFY NAME##_changed) \
Q_SIGNALS: \
    void NAME##_changed(TYPE value); \
public: \
    Q_INVOKABLE static TYPE NAME() \
    {\
        auto thisInstance = instance(); \
        QMutexLocker locker(&thisInstance->m_##NAME##_mutex); \
        return thisInstance->mSettings->value(QStringLiteral(PATH"/"#NAME), DEFAULT_VALUE).value<TYPE>();\
    }\
    Q_INVOKABLE static void set_##NAME(const TYPE & NAME) \
    {\
        auto thisInstance = instance(); \
        QMutexLocker locker(&thisInstance->m_##NAME##_mutex); \
        thisInstance->mSettings->setValue(PATH"/"#NAME, NAME);\
        thisInstance->mSettings->sync();\
        emit thisInstance->NAME##_changed(NAME);\
    } \
private: \
    QMutex m_##NAME##_mutex;

//#define SETTING_PATH_QOBJECT(PATH, NAME)\
//public: \
//    Q_INVOKABLE void read_##NAME(QObject *object) \
//    {\
//        auto propertyNames = QObjectHelper::propertyNames(object); \
//        QMutexLocker locker(&m_##NAME##_mutex); \
//        QVariant value; \
//        for (const QString &propertyName : propertyNames) { \
//            value = mSettings->value(QString(QStringLiteral(PATH"/"#NAME"/%1")).arg(propertyName)); \
//            if (!value.isValid()) { \
//                continue; \
//            } \
//            object->setProperty(propertyName.toUtf8().constData(), value); \
//        } \
//    }\
//    Q_INVOKABLE void save_##NAME(QObject *object) \
//    {\
//        auto propertyNames = QObjectHelper::propertyNames(object); \
//        QMutexLocker locker(&m_##NAME##_mutex); \
//        for (const QString &propertyName : propertyNames) { \
//            mSettings->setValue(QString(QStringLiteral(PATH"/"#NAME"/%1")).arg(propertyName), \
//                                object->property(propertyName.toUtf8().constData()));\
//        } \
//        mSettings->sync();\
//        emit NAME##_changed(object);\
//    } \
//    Q_SIGNALS: \
//        void NAME##_changed(QObject *object); \
//private: \
//    QMutex m_##NAME##_mutex;

//#include <QtCore/qglobal.h>

//#if defined(SETTINGS_LIBRARY)
//#  define SETTINGSSHARED_EXPORT Q_DECL_EXPORT
//#else
//#  define SETTINGSSHARED_EXPORT Q_DECL_IMPORT
//#endif

class /*SETTINGSSHARED_EXPORT*/ AbstractSettings : public QObject
{
    Q_OBJECT

public:
    void init();

    bool contains(const QString &name) const;
    QVariant value(const QString &name) const;

protected:
    AbstractSettings(const QString &filePath);
    virtual ~AbstractSettings() {}
    QSettings *mSettings;
};

#endif // ABSTRACTSETTINGS_H
