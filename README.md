# Usage

Let's consider that Settings dir is inside Project/submodules.

```
set(projectDir ${CMAKE_CURRENT_LIST_DIR})

set(submodulesDir ${projectDir}/submodules)

set(settingsInclude ${submodulesDir}/Settings/src/)

include_directories(
    ...
    ${settingsInclude}
)

file(GLOB_RECURSE settingsFiles ${settingsInclude}/*.cpp ${settingsInclude}/*.h)

add_executable(SomeBinaryName ${sourceFiles} ${settingsFiles})
```
